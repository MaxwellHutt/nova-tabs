<?php

namespace R64\NovaTab;

use Laravel\Nova\Fields\FieldCollection;
use Laravel\Nova\Panel;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Resource;

trait Tabs
{
    public function updateFields(NovaRequest $request)
    {
        $updateFields = parent::updateFields($request);
        if (!$request->isMethod('get')) {
            return $updateFields;
        }
        $updateFields = $this->availableTabs($request, $updateFields);
        return $updateFields;
    }

    /**
     * Prepare the resource for JSON serialization.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function serializeForDetail(NovaRequest $request, Resource $resource)
    {
        $detailFields = parent::serializeForDetail($request, $resource);
        $detailFields['fields'] = $this->availableTabs($request, $detailFields['fields']);
        return $detailFields;
    }

    /**
     * Resolve the creation fields.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return FieldCollection
     */
    public function creationFields(NovaRequest $request)
    {
        $creationFields = parent::creationFields($request);
        if (!$request->isMethod('get')) {
            return $creationFields;
        }
        return $this->availableTabs($request, $creationFields);
    }

    /**
     * Get the panels that are available for the given request.
     *
     * @param  \Laravel\Nova\Http\Requests\ResourceDetailRequest  $request
     * @return FieldCollection
     */
    public function availableTabs(NovaRequest $request, $fields)
    {
        $tabs = (new FieldCollection(array_values($this->fields($request))))
            ->whereInstanceOf(NovaTab::class)->values();
        if (count($tabs) > 0) {
            if ($fields instanceof FieldCollection) {
                $fields = $fields->all();
            }
            $this->assignFieldsToTabs($request, $fields);
            return new FieldCollection([
                (NovaTabs::make('tabs'))
                    ->withMeta(['fields' => array_values($fields)])
            ]);
        }
        return $fields;
    }

    protected function assignFieldsToTabs(NovaRequest $request, $fields)
    {
        foreach ($fields as $field) {
            $name = $field->meta['tab'] ?? Panel::defaultNameForCreate($request->newResource());
            $field->meta['tab'] = [
                'name' => $name,
                'html' => $field->meta['tabHTML'] ?? $name,
//                'error' => $field->meta['hasError']
            ];
        }

        return $fields;
    }

    /**
     * Assign the fields with the given panels to their parent panel.
     *
     * @param  string                           $label
     * @param  FieldCollection   $panels
     * @return FieldCollection
     */
    protected function assignToPanels($label, FieldCollection $panels)
    {
        return $panels->map(function ($field) use ($label) {
            if (!is_array($field) && !$field->panel) {
                $field->panel = $label;
            }
            return $field;
        });
    }
}
